using UnityEngine;
using System;

public class Clock : MonoBehaviour
{
    const float hoursToDegrees = 30f, minutesToDegrees = 6f, secondsToDegrees = 6f;
    //Fields of our GameObjects we want to refer to
    //SerializeField allow us to link our GameObjects to those fields, by letting them private
    [SerializeField]
    Transform hoursPivot = default;
    [SerializeField]
    Transform minutesPivot = default;
    [SerializeField]
    Transform secondsPivot = default;

    //Awake method called after component has been created (or loaded)
    //Update method called every frame as ling as we stay in play mode
    void Update() {
        TimeSpan now = DateTime.Now.TimeOfDay;
        hoursPivot.localRotation = Quaternion.Euler(0f, 0f, hoursToDegrees * (float)now.TotalHours);
        minutesPivot.localRotation = Quaternion.Euler(0f, 0f, minutesToDegrees * (float)now.TotalMinutes);
        secondsPivot.localRotation = Quaternion.Euler(0f, 0f, secondsToDegrees * (float)now.TotalSeconds);
    }
}